﻿using System;

namespace Verdieping
{
    public class Book
    {
        public string author;
        public string title;
        public int pages;
        private string rating;

        public Book()
        {
            
        }

        public Book(string aAuthor, string aTitle, int aPages, string aRating)
        {
            author = aAuthor;
            title = aTitle;
            pages = aPages;
            Rating = aRating;
        }

        public string Rating
        {
            get
            {
                return rating;
            }
            set
            {
                if (value == "G" || value == "PG" || value == "PG-12" || value == "PG-13" || value == "R" || value == "NR")
                {
                    rating = value;
                }
                else
                {
                    rating = "NR";
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Concurrent;
using System.Reflection.Metadata.Ecma335;

namespace Verdieping
{
    class Program
    {
        public class Person
        {
            public string Name;

            public void Introduce(string to)
            {
                Console.WriteLine("Hi {0}, I'm {1}.", to, Name);
            }

            public static Person Parse(string str)
            {
                Person person = new Person();
                person.Name = str;

                return person;
            }
        }
        static void Main(string[] args)
        {
            Person person = Person.Parse("Jason");
            person.Introduce("Tatsuo");

            Console.WriteLine();

            Second();
            Third();
        }


        public static void Second()
        {
            Customer customer = new Customer();
            customer.Id = 1;
            customer.Name = "Tatsuo";

            Order order = new Order();
            customer.Orders.Add(order);

            Console.WriteLine(customer.Id);
            Console.WriteLine(customer.Name);

            Console.WriteLine();
        }

        public static void Third()
        {
            Book book1 = new Book("John Flanagan", "The Ranger's apprentice", 300, "R");
            Book book2 = new Book("Robert Ludlum", "The Bourne Ultimatum", 667, "PG-12");
            Book book3 = new Book();

            Console.WriteLine(book1.Rating);
            Console.ReadLine();
        }
    } 
}
